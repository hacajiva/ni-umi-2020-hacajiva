# NI-UMI-2020-hacajiva

## Splňovanie obmedzení
Vybrala som si úlohu zafarbovania mapy Austrálie a pridala Teritórium hlavného
mesta (ACT).

Implementovala som tieto algoritmy:
- Backtracking
- Backjumping
- Backtracking s hranovou konzistenciou
- Dynamický backtracking

Zo súboru `states.geojson` som získala dáta pre vykreslenie jednotlivých štátov
do zafarbeného grafu. Tak isto som počítala počet ohodnotení pre každý algoritmus. 

## Automatické uvažovanie
Vypracovala som úlohy 3, 8 a 9